package net.smartdrive.geodimension

import org.apache.spark.sql.SparkSession
import org.apache.spark.storage.StorageLevel
import org.datasyslab.geospark.enums.{FileDataSplitter, GridType, IndexType}
import org.datasyslab.geospark.spatialRDD.PointRDD



class RawPointData(spark: SparkSession, geoDimensionConfig: GeoDimensionConfig) {

  def read(startDateString: String, endDateString: String, numRows: Int): PointRDD = {
    // read input df and create point df
    val inputPath = geoDimensionConfig.inputFactTripSliceDataPath
    val filterCondition =
      s"dim_start_date_key_partition >= $startDateString and dim_start_date_key_partition <= $endDateString"
    val df = spark.read.load(inputPath).filter(filterCondition)
//    val df = spark.read.format("avro").load("s3://sdsidwarchive/r&d_Geodimension_QA/20190215.avro")
    if (numRows > 0) {
      df.limit(numRows).createOrReplaceTempView("rawDF")
    } else {
      df.createOrReplaceTempView("rawDF")
    }

    // remove NULL lat/lng
    val temp_file_name = s"points_${startDateString}_$endDateString.csv"
    val x = df
      .select("slice_end_longitude", "slice_end_latitude", "trip_slice_id")
      .filter("slice_end_longitude is not null")
      .filter("slice_end_latitude is not null")
    x.write.mode("overwrite").format("com.databricks.spark.csv").save(temp_file_name)

    val partitioningScheme = GridType.QUADTREE
    val idx = IndexType.RTREE
    val numPartitions = 1024

    val leftRDD = new PointRDD(spark.sparkContext,
      temp_file_name,
      0,
      FileDataSplitter.CSV,
      true,
      numPartitions)

    leftRDD.analyze()
    leftRDD.spatialPartitioning(partitioningScheme)
    leftRDD.buildIndex(idx, true)
    leftRDD.indexedRDD.persist(StorageLevel.MEMORY_ONLY)
    leftRDD.spatialPartitionedRDD.persist(StorageLevel.MEMORY_ONLY)
    val c1 = leftRDD.spatialPartitionedRDD.count()
    val c2 = leftRDD.indexedRDD.count()
    leftRDD.rawSpatialRDD.unpersist()
    leftRDD.spatialPartitionedRDD.unpersist()

    leftRDD
  }
}
