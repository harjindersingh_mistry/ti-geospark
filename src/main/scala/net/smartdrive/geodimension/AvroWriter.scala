package net.smartdrive.geodimension

import org.apache.spark.sql.{SparkSession, _}
import org.apache.spark.sql.functions.{expr, _}


class AvroWriter(spark: SparkSession, geoDimensionConfig: GeoDimensionConfig) {

  def write(dateString: String): Unit = {
    val inputPath = s"${geoDimensionConfig.outputDataPath}/demo/$dateString"
    val df = spark.read.load(inputPath)

    val outputPath = s"${geoDimensionConfig.outputDataPath}/avro/ti-gd-$dateString.avro"
    df.write.format("avro").option("compression", "uncompressed").partitionBy("dim_start_date_key").save(outputPath)
  }
}
