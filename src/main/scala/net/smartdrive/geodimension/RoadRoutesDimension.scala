package net.smartdrive.geodimension

import java.util
import java.util.regex.Pattern

import com.vividsolutions.jts.geom.{Geometry, Point}
import org.apache.spark.api.java.function.FlatMapFunction
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.storage.StorageLevel
import org.datasyslab.geospark.formatMapper.GeoJsonReader
import org.datasyslab.geospark.spatialOperator.JoinQuery
import org.datasyslab.geospark.spatialRDD.PointRDD
import org.apache.spark.sql.functions.row_number
import org.apache.spark.sql.expressions.Window

import scala.collection.JavaConverters._


class RoadRoutesDimension(spark: SparkSession, geoDimensionConfig: GeoDimensionConfig) {

  def decorateOutputPath(outputPath: String, startDateString: String, endDateString: String): String = {
    s"$outputPath/road_routes_info/${startDateString}_$endDateString"
  }

  def addColumn(leftRDD: PointRDD, startDateString: String, endDateString: String): Unit = {
    val rightRDD = GeoJsonReader.readToGeometryRDD(spark.sparkContext,
      geoDimensionConfig.roadRoutesShapeFilePath, true, true)
    rightRDD.analyze()
    rightRDD.spatialPartitioning(leftRDD.getPartitioner)
    rightRDD.spatialPartitionedRDD.persist(StorageLevel.MEMORY_ONLY)
    val c3 = rightRDD.spatialPartitionedRDD.count()
    rightRDD.rawSpatialRDD.unpersist()

    val result = JoinQuery.SpatialJoinQuery(leftRDD, rightRDD, true, true)

    val rdd = result.flatMap(new FlatMapFunction[(Geometry, util.HashSet[Point]), Row]() {
      override def call(t: (Geometry, util.HashSet[Point])): util.Iterator[Row] = {
        val arrValues = t._1.getUserData.asInstanceOf[String].split("\t")

        val rttyp =  arrValues(1)
        val roadName =
          if (rttyp == "I") {
            val pattern = Pattern.compile("\\w- \\d*")
            val m = pattern.matcher(arrValues(0))
            if (m.find())
              m.group()
            else
              arrValues(0)
          }
          else {
            arrValues(0)
          }

        val mtfcc = arrValues(2)
        val rttyp_rank = {
          rttyp match {
            case "I" => "1"
            case "U" => "2"
            case "S" => "3"
            case "C" => "4"
            case "M" => "5"
            case "O" => "6"
            case _ => "7"
          }
        }

        val arr: Array[Row] =
          t._2.toArray().map(x => Row(x.toString.split("\t")(1).toLong, roadName, rttyp, mtfcc, rttyp_rank))

        arr.iterator.asJava
      }
    })

    val someSchema = List(
      StructField("trip_slice_id", LongType, nullable = true),
      StructField("road_name", StringType, nullable = true),
      StructField("road_rttyp", StringType, nullable = true),
      StructField("road_mtfcc", StringType, nullable = true),
      StructField("rttyp_rank", StringType, nullable = true)
    )
    val df = spark.sqlContext.createDataFrame(rdd, StructType(someSchema))
    val w = Window.partitionBy("trip_slice_id").orderBy("rttyp_rank")
    val dfTop = df.withColumn("rn", row_number.over(w)).filter("rn = 1").drop("rn")

    val outputPath = decorateOutputPath(geoDimensionConfig.outputDataPath, startDateString, endDateString)
    dfTop.write.mode("overwrite").parquet(outputPath)
  }
}

