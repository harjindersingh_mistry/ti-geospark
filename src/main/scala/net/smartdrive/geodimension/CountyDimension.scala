package net.smartdrive.geodimension

import java.util

import com.vividsolutions.jts.geom.{Point, Polygon}
import org.apache.spark.api.java.function.FlatMapFunction
import org.apache.spark.sql.types.{LongType, StringType, StructField, StructType}
import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.storage.StorageLevel
import org.datasyslab.geospark.formatMapper.shapefileParser.ShapefileReader
import org.datasyslab.geospark.spatialOperator.JoinQuery
import org.datasyslab.geospark.spatialRDD.PointRDD

import scala.collection.JavaConverters._

class CountyDimension(spark: SparkSession, geoDimensionConfig: GeoDimensionConfig) {

  def decorateOutputPath(outputPath: String, startDateString: String, endDateString: String): String = {
    s"$outputPath/county_info/${startDateString}_$endDateString"
  }

  def addColumn(leftRDD: PointRDD, startDateString: String, endDateString: String): Unit = {
    val rightRDD = ShapefileReader.readToPolygonRDD(spark.sparkContext, geoDimensionConfig.countyShapeFilePath)
    rightRDD.analyze()
    rightRDD.spatialPartitioning(leftRDD.getPartitioner)
    rightRDD.spatialPartitionedRDD.persist(StorageLevel.MEMORY_ONLY)
    val c3 = rightRDD.spatialPartitionedRDD.count()
    rightRDD.rawSpatialRDD.unpersist()

    val result = JoinQuery.SpatialJoinQuery(leftRDD, rightRDD, true, false)
    val rdd = result.flatMap(new FlatMapFunction[(Polygon, util.HashSet[Point]), Row]() {
      override def call(t: (Polygon, util.HashSet[Point])): util.Iterator[Row] = {
        val countyName =  t._1.getUserData.asInstanceOf[String].split("\t")(5)
        val arr: Array[Row] =
          t._2.toArray().map(x => Row(x.toString.split("\t")(1).toLong, countyName))

        arr.iterator.asJava
      }
    })

    val someSchema = List(
      StructField("trip_slice_id", LongType, nullable = true),
      StructField("county_name", StringType, nullable = true)
    )
    val df = spark.sqlContext.createDataFrame(rdd, StructType(someSchema))
    val outputPath = decorateOutputPath(geoDimensionConfig.outputDataPath, startDateString, endDateString)
    df.write.mode("overwrite").parquet(outputPath)
  }
}
