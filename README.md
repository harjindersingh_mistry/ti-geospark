# ti-geospark
 by: Bharat Jain & Harjindersingh Mistry, SmartDrive Systems       

## Project Docs

## Synopsis
This module augments the input data with geo-spatial information state, county, zip-code, urban area and road routes.
For mapping a (latitude, longitude) pair to geo-spatial location information, it depends on various Shape files
published by the US Census department [1]. External lib GeoSpark [2] is used to process spatial data.

## References:
[1] https://www.census.gov/geo/maps-data/data/tiger-cart-boundary.html

[2] http://geospark.datasyslab.org

## Usage:
First, build the JAR file (ti-geospark_2.11-0.1.jar) by using SBT. Then spark job can be triggered as follows:
```
spark-submit \
  --packages org.datasyslab:geospark:1.2.0,org.datasyslab:geospark-sql_2.3:1.2.0,org.yaml:snakeyaml:1.8,org.apache.spark:spark-avro_2.11:2.4.0 \
  --class net.smartdrive.geodimension.GeoDimensionApp \
  --master yarn --deploy-mode client \
  --driver-memory 8g --executor-memory 8g --num-executors 60 --executor-cores 2 \
  --conf "spark.kryoserializer.buffer.max=2047" \
  ti-geospark_2.11-0.1.jar <start_date> <end_date> <keyword>
```

Here,
<start_date>: a date in YYYYMMDD format e.g. 20181218 for Dec 18, 2018
<end_date>: a date in YYYYMMDD format e.g. 20181218 for Dec 18, 2018
<keyword>: a keyword that suggests action. Valid keywords are: state, county, urban_area, zip_code, road_routes, merge, final

The following action is taken for the given keyword:
```
'state', 'county', 'urban_area', 'zip_code' and 'road_routes' => add respective geo dimensions
'merge' => merges the intermediate results and consolidates them into one final data set
'final' => all-in-one action i.e. first, add all five geo dimensions and merge the intermediate results
```

The input and output data-paths are specified via config file. The default config are as follows. The input fact trip
slice table is taken as a Parquet file mentioned in `inputFactTripSliceDataPath` config. The intermediate output is
saved in multiple locations and the final output path is specified in `finalDataPath` config.
```
inputFactTripSliceDataPath: s3://sdsidwarchive/sds_CloudDataWarehouse/parquet_fact_trip_slice
tempDataPath: s3://sdsidwarchive/sds_CloudDataWarehouse/geo_dimension/temp
outputDataPath: s3://sdsidwarchive/sds_CloudDataWarehouse/geo_dimension/output
stateShapeFilePath: s3n://sdsi.int.analytics.geodata.locationandroad.shape/Shape Files/US - 2018/State/
countyShapeFilePath: s3n://sdsi.int.analytics.geodata.locationandroad.shape/Shape Files/US - 2018/County/
urbanAreaShapeFilePath: s3n://sdsi.int.analytics.geodata.locationandroad.shape/Shape Files/US - 2018/Urban Areas/
zipCodeShapeFilePath: s3://sdsi.int.analytics.geodata.locationandroad.shape/Shape Files/US - 2018/Zip Code/
roadRoutesShapeFilePath: s3://sdsi.int.analytics.geodata.locationandroad.shape/Shape Files/2017_all_roads_20mts
finalDataPath: s3://sdsidwarchive/sds_CloudDataWarehouse/geography_roadroute/output/result
```

Currently, the spark job is optimized to run for a week's worth of data i.e. roughly 70 million rows of fact-trip-slice
table. We have used the following script to add geo dimension information for multiple weeks/months:

```
arr1=(20180701 20180708 20180715 20180722 20180729 20180805)
arr2=(20180708 20180715 20180722 20180729 20180805 20180812)
for i in ${!arr1[@]}
do
    start_week=${arr1[$i]}
    end_week=${arr2[$i]}
    end_week="$(($end_week - 1))"

    echo "----------------------------------"
    echo Running job for $start_week:
    echo "----------------------------------"
    echo $start_week $end_week
    spark-submit --packages org.datasyslab:geospark:1.2.0,org.datasyslab:geospark-sql_2.3:1.2.0,org.yaml:snakeyaml:1.8,org.apache.spark:spark-avro_2.11:2.4.0 --class net.smartdrive.geodimension.GeoDimensionApp --master yarn --deploy-mode client --driver-memory 8g --executor-memory 8g --num-executors 60 --executor-cores 2 --conf "spark.kryoserializer.buffer.max=2047" ti-geospark_2.11-0.1.jar $start_week $end_week final
done
```

