arr1=(20180701 20180708 20180715 20180722 20180729 20180805)
arr2=(20180708 20180715 20180722 20180729 20180805 20180812)
for i in ${!arr1[@]}
do
    start_week=${arr1[$i]}
    end_week=${arr2[$i]}
    end_week="$(($end_week - 1))"

    echo "----------------------------------"
    echo Running job for $start_week:
    echo "----------------------------------"
    echo $start_week $end_week
    spark-submit --packages org.datasyslab:geospark:1.2.0,org.datasyslab:geospark-sql_2.3:1.2.0,org.yaml:snakeyaml:1.8,org.apache.spark:spark-avro_2.11:2.4.0 --class net.smartdrive.geodimension.GeoDimensionApp --master yarn --deploy-mode client --driver-memory 8g --executor-memory 8g --num-executors 60 --executor-cores 2 --conf "spark.kryoserializer.buffer.max=2047" ti-geospark_2.11-0.1.jar $start_week $end_week final
done