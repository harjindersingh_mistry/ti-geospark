package net.smartdrive.geodimension

import org.apache.spark.sql.{SparkSession, _}


class DataChunks(spark: SparkSession, geoDimensionConfig: GeoDimensionConfig) {

  def readPartition(geoDimString: String, startDateString: String, endDateString: String): DataFrame = {
    val inputPath = s"${geoDimensionConfig.outputDataPath}/${geoDimString}_info" + s"/${startDateString}_$endDateString"
    spark.read.load(inputPath)
  }

  def merge(startDateString: String, endDateString: String): Unit = {
    val inputPath = geoDimensionConfig.inputFactTripSliceDataPath
    val filterCondition =
      s"dim_start_date_key_partition >= $startDateString and dim_start_date_key_partition <= $endDateString"
    val df = spark.read.load(inputPath).filter(filterCondition)
//     val df = spark.read.format("avro").load("s3://sdsidwarchive/r&d_Geodimension_QA/20190215.avro")
    df.cache()
    df.createOrReplaceTempView("inputDF")

    val stateDF = readPartition("state", startDateString, endDateString)
    stateDF.createOrReplaceTempView("stateDF")

    val countyDF = readPartition("county", startDateString, endDateString)
    countyDF.createOrReplaceTempView("countyDF")

    val urbanAreaDF = readPartition("urban_area", startDateString, endDateString)
    urbanAreaDF.createOrReplaceTempView("urbanAreaDF")

    val zipCodeDF = readPartition("zip_code", startDateString, endDateString)
    zipCodeDF.createOrReplaceTempView("zipCodeDF")

    val roadRoutesDF = readPartition("road_routes", startDateString, endDateString)
    roadRoutesDF.createOrReplaceTempView("roadRoutesDF")

    val tempDF = spark.sql(
      """
        |SELECT
        |  stateDF.trip_slice_id,
        |  stateDF.state_code,
        |  stateDF.state_name,
        |  countyDF.county_name,
        |  urbanAreaDF.urban_area_code,
        |  urbanAreaDF.urban_area_name,
        |  zipCodeDF.zip_code,
        |  roadRoutesDF.road_name,
        |  roadRoutesDF.road_rttyp,
        |  roadRoutesDF.road_mtfcc,
        |  roadRoutesDF.rttyp_rank
        |FROM stateDF
        |LEFT JOIN countyDF ON stateDF.trip_slice_id = countyDF.trip_slice_id
        |LEFT JOIN urbanAreaDF ON stateDF.trip_slice_id = urbanAreaDF.trip_slice_id
        |LEFT JOIN zipCodeDF ON stateDF.trip_slice_id = zipCodeDF.trip_slice_id
        |LEFT JOIN roadRoutesDF ON stateDF.trip_slice_id = roadRoutesDF.trip_slice_id
      """.stripMargin)
    tempDF.createOrReplaceTempView("tempDF")

//    |  inputDF.dim_start_date_key as dim_start_date_key_partition,
    val outputDF = spark.sql(
      """
        |SELECT
        |  inputDF.*,
        |  tempDF.state_code,
        |  tempDF.state_name,
        |  tempDF.county_name,
        |  tempDF.urban_area_code,
        |  tempDF.urban_area_name,
        |  tempDF.zip_code,
        |  tempDF.road_name,
        |  tempDF.road_rttyp,
        |  tempDF.road_mtfcc,
        |  tempDF.rttyp_rank
        |FROM inputDF LEFT JOIN tempDF
        |ON inputDF.trip_slice_id = tempDF.trip_slice_id
      """.stripMargin)
      .na.fill("R", Seq("urban_area_code"))
      .na.fill("Rural", Seq("urban_area_name"))
      .na.fill("(none)", Seq("road_name"))
      .na.fill("(none)", Seq("road_rttyp"))
      .na.fill("(none)", Seq("road_mtfcc"))
      .na.fill("7", Seq("rttyp_rank"))

    val outputPath = geoDimensionConfig.finalDataPath
    outputDF
      .write.mode("append")
      .format("avro")
      .option("compression", "uncompressed")
      .partitionBy("dim_start_date_key_partition")
      .save(outputPath)
  }

  /**
    * Merges only road routes dim with four other dim: state, county, urban_area and zip_code
    * that are already computed and saved previously.
    *
    * @param startDateString start date of time period
    * @param endDateString end date of time period
    */
  def mergeRoadRoutes(startDateString: String, endDateString: String): Unit = {
    val inputPath = s"${geoDimensionConfig.outputDataPath}/result"
    val filterCondition =
      s"dim_start_date_key_partition >= $startDateString and dim_start_date_key_partition <= $endDateString"
    val df = spark.read.format("avro").load(inputPath).filter(filterCondition)

    df.cache()
    df.createOrReplaceTempView("inputDF")

    val roadRoutesDF = readPartition("road_routes", startDateString, endDateString)
    roadRoutesDF.createOrReplaceTempView("roadRoutesDF")

    val outputDF = spark.sql(
      """
        |SELECT
        |  inputDF.*,
        |  roadRoutesDF.road_name,
        |  roadRoutesDF.road_rttyp,
        |  roadRoutesDF.road_mtfcc,
        |  roadRoutesDF.rttyp_rank
        |FROM inputDF LEFT JOIN roadRoutesDF ON inputDF.trip_slice_id = roadRoutesDF.trip_slice_id
      """.stripMargin)
      .na.fill("(none)", Seq("road_name"))
      .na.fill("(none)", Seq("road_rttyp"))
      .na.fill("(none)", Seq("road_mtfcc"))
      .na.fill("7", Seq("rttyp_rank"))

    val outputPath = geoDimensionConfig.finalDataPath
    outputDF
      .write.mode("append")
      .format("avro")
      .option("compression", "uncompressed")
      .partitionBy("dim_start_date_key_partition")
      .save(outputPath)
  }

}
