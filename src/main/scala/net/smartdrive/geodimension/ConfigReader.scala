package net.smartdrive.geodimension

import java.io.{File, FileInputStream}
import java.util

import org.yaml.snakeyaml.Yaml

object ConfigReader {
  def toGeoDimensionConfig(map: util.LinkedHashMap[String, String]): GeoDimensionConfig = {
    val inputFactTripSliceDataPath: String = map.get("inputFactTripSliceDataPath")
    val tempDataPath: String = map.get("tempDataPath")
    val outputDataPath: String = map.get("outputDataPath")
    val stateShapeFilePath: String = map.get("stateShapeFilePath")
    val countyShapeFilePath: String = map.get("countyShapeFilePath")
    val urbanAreaShapeFilePath: String = map.get("urbanAreaShapeFilePath")
    val zipCodeShapeFilePath: String = map.get("zipCodeShapeFilePath")
    val roadRoutesShapeFilePath: String = map.get("roadRoutesShapeFilePath")
    val finalDataPath: String = map.get("finalDataPath")

    GeoDimensionConfig(inputFactTripSliceDataPath,
      tempDataPath,
      outputDataPath,
      stateShapeFilePath,
      countyShapeFilePath,
      urbanAreaShapeFilePath,
      zipCodeShapeFilePath,
      roadRoutesShapeFilePath,
      finalDataPath)
  }

  def read(fileName: String): GeoDimensionConfig = {
    val input =
      if (fileName.isEmpty) {
        getClass.getResourceAsStream("/defaultConfig.yaml")
      } else {
        new FileInputStream(new File(fileName))
      }
    val yaml = new Yaml
    val obj = yaml.load(input)
    toGeoDimensionConfig(obj.asInstanceOf[util.LinkedHashMap[String, String]])
  }
}
