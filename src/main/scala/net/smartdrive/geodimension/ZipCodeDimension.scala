package net.smartdrive.geodimension

import java.util

import com.vividsolutions.jts.geom.{Point, Polygon}
import org.apache.spark.api.java.function.FlatMapFunction
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.storage.StorageLevel
import org.datasyslab.geospark.formatMapper.shapefileParser.ShapefileReader
import org.datasyslab.geospark.spatialOperator.JoinQuery
import org.datasyslab.geospark.spatialRDD.PointRDD
import org.apache.spark.sql.functions.row_number
import org.apache.spark.sql.expressions.Window

import scala.collection.JavaConverters._


class ZipCodeDimension(spark: SparkSession, geoDimensionConfig: GeoDimensionConfig) {

  def decorateOutputPath(outputPath: String, startDateString: String, endDateString: String): String = {
    s"$outputPath/zip_code_info/${startDateString}_$endDateString"
  }

  def addColumn(leftRDD: PointRDD, startDateString: String, endDateString: String): Unit = {
    val rightRDD = ShapefileReader.readToPolygonRDD(spark.sparkContext, geoDimensionConfig.zipCodeShapeFilePath)
    rightRDD.analyze()
    rightRDD.spatialPartitioning(leftRDD.getPartitioner)
    rightRDD.spatialPartitionedRDD.persist(StorageLevel.MEMORY_ONLY)
    val c3 = rightRDD.spatialPartitionedRDD.count()
    rightRDD.rawSpatialRDD.unpersist()

    val result = JoinQuery.SpatialJoinQuery(leftRDD, rightRDD, true, false)
    val rdd = result.flatMap(new FlatMapFunction[(Polygon, util.HashSet[Point]), Row]() {
      override def call(t: (Polygon, util.HashSet[Point])): util.Iterator[Row] = {
        val zipCode =  t._1.getUserData.asInstanceOf[String].split("\t")(1)
        val arr: Array[Row] =
          t._2.toArray().map(x => Row(x.toString.split("\t")(1).toLong, zipCode))

        arr.iterator.asJava
      }
    })

    val someSchema = List(
      StructField("trip_slice_id", LongType, nullable = true),
      StructField("zip_code", StringType, nullable = true)
    )

    val df = spark.sqlContext.createDataFrame(rdd, StructType(someSchema))
//    df.cache()
//    val w = Window.partitionBy("trip_slice_id").orderBy("zip_code")
//    val dedupeDF = df.withColumn("rn", row_number.over(w)).filter("rn = 1").drop("rn")
    val outputPath = decorateOutputPath(geoDimensionConfig.outputDataPath, startDateString, endDateString)
    df.write.mode("overwrite").parquet(outputPath)
  }
}
