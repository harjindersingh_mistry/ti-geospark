package net.smartdrive.geodimension

case class GeoDimensionConfig(inputFactTripSliceDataPath: String,
                              tempDataPath: String,
                              outputDataPath: String,
                              stateShapeFilePath: String,
                              countyShapeFilePath: String,
                              urbanAreaShapeFilePath: String,
                              zipCodeShapeFilePath: String,
                              roadRoutesShapeFilePath: String,
                              finalDataPath: String)
