package net.smartdrive.geodimension

import org.apache.spark.serializer.KryoSerializer
import org.apache.spark.sql.SparkSession
import org.datasyslab.geospark.serde.GeoSparkKryoRegistrator
import org.datasyslab.geosparksql.utils.GeoSparkSQLRegistrator

case class InputArgs(startDateString: String,
                     endDateString: String,
                     action: String,
                     numPartitions: Int,
                     numRows: Int = 0,
                     configFileName: String = "")

object GeoDimensionApp {

  def parseArgs(args: Array[String]): InputArgs = {
    if (args.length < 3) {
      val msg: String =
        """
          |Please provide the following inputs:
          |<start_date>: in YYYYMMDD format e.g. 20181218 for Dec 18, 2018
          |<end_date>: in YYYYMMDD format e.g. 20181218 for Dec 18, 2018
          |<keyword>: one of the following keywords {state, county, urban_area, zip_code, road_routes, merge, final}
        """.stripMargin
      throw new IllegalArgumentException(msg)
    }

    val startDateString = args(0)
    val endDateString = args(1)
    val action = args(2)
    InputArgs(startDateString, endDateString, action, 0)
  }

  def main(args: Array[String]) {
    val inputs: InputArgs = parseArgs(args)
    println(inputs)
    val geoDimensionConfig = ConfigReader.read(inputs.configFileName)
    println(geoDimensionConfig)
    val appName = "TI-GD-" + inputs.action + "-" + inputs.startDateString + "-" + inputs.endDateString

    // setup and register geo spark UDFs
    val spark = SparkSession.builder()
      .appName(appName)
      .config("spark.serializer", classOf[KryoSerializer].getName)
      .config("spark.kryo.registrator", classOf[GeoSparkKryoRegistrator].getName)
      .getOrCreate()
    val sc = spark.sparkContext
    GeoSparkSQLRegistrator.registerAll(spark)

    // read raw data and extract points
    val rawPointData = new RawPointData(spark, geoDimensionConfig)
    val pointRDD = rawPointData.read(inputs.startDateString, inputs.endDateString, inputs.numRows)

    // take the necessary action
    inputs.action match {
      case "state" =>
        val stateDimension = new StateDimension(spark, geoDimensionConfig)
        stateDimension.addColumn(pointRDD, inputs.startDateString, inputs.endDateString)

      case "county" =>
        val countyDimension = new CountyDimension(spark, geoDimensionConfig)
        countyDimension.addColumn(pointRDD, inputs.startDateString, inputs.endDateString)

      case "urban_area" =>
        val urbanAreaDimension = new UrbanAreaDimension(spark, geoDimensionConfig)
        urbanAreaDimension.addColumn(pointRDD, inputs.startDateString, inputs.endDateString)

      case "zip_code" =>
        val zipCodeDimension = new ZipCodeDimension(spark, geoDimensionConfig)
        zipCodeDimension.addColumn(pointRDD, inputs.startDateString, inputs.endDateString)

      case "road_routes" =>
        val roadRoutesDimension = new RoadRoutesDimension(spark, geoDimensionConfig)
        roadRoutesDimension.addColumn(pointRDD, inputs.startDateString, inputs.endDateString)

      case "merge" =>
        val dataChunks = new DataChunks(spark, geoDimensionConfig)
        dataChunks.merge(inputs.startDateString, inputs.endDateString)

      case "merge_road_routes" =>
        val roadRoutesDimension = new RoadRoutesDimension(spark, geoDimensionConfig)
        roadRoutesDimension.addColumn(pointRDD, inputs.startDateString, inputs.endDateString)

        val dataChunks = new DataChunks(spark, geoDimensionConfig)
        dataChunks.mergeRoadRoutes(inputs.startDateString, inputs.endDateString)

      case "final" =>
        val stateDimension = new StateDimension(spark, geoDimensionConfig)
        stateDimension.addColumn(pointRDD, inputs.startDateString, inputs.endDateString)

        val countyDimension = new CountyDimension(spark, geoDimensionConfig)
        countyDimension.addColumn(pointRDD, inputs.startDateString, inputs.endDateString)

        val urbanAreaDimension = new UrbanAreaDimension(spark, geoDimensionConfig)
        urbanAreaDimension.addColumn(pointRDD, inputs.startDateString, inputs.endDateString)

        val zipCodeDimension = new ZipCodeDimension(spark, geoDimensionConfig)
        zipCodeDimension.addColumn(pointRDD, inputs.startDateString, inputs.endDateString)

        val roadRoutesDimension = new RoadRoutesDimension(spark, geoDimensionConfig)
        roadRoutesDimension.addColumn(pointRDD, inputs.startDateString, inputs.endDateString)

        val dataChunks = new DataChunks(spark, geoDimensionConfig)
        dataChunks.merge(inputs.startDateString, inputs.endDateString)

      case _ =>
        throw new IllegalArgumentException(s"Unknown action keyword ${inputs.action}!")
    }

    spark.stop()
  }
}
