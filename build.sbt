name := "ti-geospark"

version := "0.1"

scalaVersion := "2.11.12"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-sql" % "2.4.0",
  "org.datasyslab" % "geospark" % "1.2.0" % "provided",
  "org.datasyslab" % "geospark-sql_2.3" % "1.2.0",
  "org.yaml" % "snakeyaml" % "1.8"
)
